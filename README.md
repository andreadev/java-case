# A technical exercise

Development of a WEB API for managing contacts in a company. 

## Features

- Creation of a contact.
- Updating a contact.
- Delete a contact.
- Creation of an enterprise.
- Updating an enterprise.
- Add a contact to an enterprise

## Business constraints: 

- A contact must have an address, a last name and a first name
- A contact works in one or more companies
- A company has an address and a VAT number
- A contact can be employed or freelance
    - If he is a freelance, he must have a VAT number. 
    
## Technical constraints: 

Synchronous WEB API REST (JSON)
- Using Spring Boot
- Most maintainable architecture possible.
- JPA
- The database is a DB in memory (HSQL or H2)
- Unit tests not required.
- The API should be exposed and searchable using Swagger UI
- Repo of public sources (like Github) or sources in a zip to send to me by email (without the
target). 

### Example of vat

0123.456.789