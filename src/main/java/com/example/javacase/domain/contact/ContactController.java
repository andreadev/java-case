package com.example.javacase.domain.contact;

import com.example.javacase.domain.contact.converter.ContactConverter;
import com.example.javacase.domain.contact.dto.ContactFormDto;
import com.example.javacase.domain.contact.entity.Contact;
import com.example.javacase.domain.contact.repository.ContactRepository;
import com.example.javacase.domain.enterprise.repository.EnterpriseRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.net.URI;

/**
 * The web service that takes care of contacts.
 *
 * @author andrea.benetti
 * @since 1.0
 */
@RestController
@RequestMapping(value = "/contact/", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin
public class ContactController {

    private final ContactRepository contactRepository;
    private final ContactConverter contactConverter;
    private final EnterpriseRepository enterpriseRepository;

    public ContactController(ContactRepository contactRepository, ContactConverter contactConverter, EnterpriseRepository enterpriseRepository) {
        this.contactRepository = contactRepository;
        this.contactConverter = contactConverter;
        this.enterpriseRepository = enterpriseRepository;
    }

    /**
     * This entry point is used to create a contact.
     *
     * @param contactFormDto the contact form
     * @return the response with the created contact
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<Contact> createContact(@Valid @RequestBody ContactFormDto contactFormDto) {

        if (contactFormDto.isFreelanceAndHaveNotVat()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Freelance must have a vat number.");
        }

        Contact contact = contactConverter.convert(contactFormDto);

        Contact contactCreated = contactRepository.save(contact);

        return ResponseEntity.created(URI.create("/contact/" + contactCreated.getId()))
                .body(contactCreated);
    }

    /**
     * This entry point is used to update a contact.
     *
     * @param contactId the id of contact to edit
     * @param contactFormDto the contact form
     * @return the response with the updated contact
     */
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<Contact> updateContact(
            @PathVariable(value = "id") Long contactId,
            @Valid @RequestBody ContactFormDto contactFormDto
    ) {

        if (contactFormDto.isFreelanceAndHaveNotVat()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Freelance must have a vat number.");
        }

        Contact contactFound = contactRepository.findById(contactId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Contact not found for this id: " + contactId));

        Contact newValue = contactConverter.convert(contactFormDto);
        contactFound.setType(newValue.getType());
        contactFound.setFirstname(newValue.getFirstname());
        contactFound.setFirstname(newValue.getFirstname());
        contactFound.setLastname(newValue.getLastname());
        contactFound.setAddress(newValue.getAddress());
        contactFound.setVat(newValue.getVat());
        Contact contact = contactRepository.save(contactFound);

        return ResponseEntity.ok(contact);

    }

    /**
     * This entry point is used to delete a contact.
     *
     * @param contactId the id of contact to delete
     * @return the response with the deleted contact
     */
    @DeleteMapping(value = "/{id}")
    public @ResponseBody ResponseEntity<Contact> deleteContact(
            @PathVariable(value = "id") Long contactId
    ) {

        Contact contactFound = contactRepository.findById(contactId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Contact not found for this id: " + contactId));

        enterpriseRepository.findAllByContactsContains(contactFound).forEach(enterprise -> {
            enterprise.removeContact(contactFound);
            enterpriseRepository.save(enterprise);
        });

        contactRepository.delete(contactFound);

        return ResponseEntity.ok(contactFound);

    }

}
