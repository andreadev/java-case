package com.example.javacase.domain.contact.converter;

import com.example.javacase.domain.contact.dto.ContactFormDto;
import com.example.javacase.domain.contact.entity.Contact;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ContactConverter implements Converter<ContactFormDto, Contact> {

    @Override
    public Contact convert(ContactFormDto contactFormDto) {
        return new Contact(contactFormDto.getType(), contactFormDto.getFirstname(), contactFormDto.getLastname(), contactFormDto.getAddress(), contactFormDto.getVat());
    }

}
