package com.example.javacase.domain.contact.dto;

import com.example.javacase.domain.contact.share.ContactType;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ContactFormDto {

    @NotNull
    private final ContactType type;
    @NotBlank
    private final String firstname;
    @NotBlank
    private final String lastname;
    @NotBlank
    private final String address;
    @Size(min = 12, max = 12, message = "la tva en belgique est sur 12 caractères")
    private String vat;

    public ContactFormDto(ContactType type, String firstname, String lastname, String address, String vat) {
        this.type = type;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
        this.vat = vat;
    }

    public ContactType getType() {
        return type;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getAddress() {
        return address;
    }

    public String getVat() {
        return vat;
    }

    public boolean isFreelanceAndHaveNotVat() {
        return ContactType.FREELANCE.equals(this.type) && vat == null;
    }
}
