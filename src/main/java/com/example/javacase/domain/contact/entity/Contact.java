package com.example.javacase.domain.contact.entity;

import com.example.javacase.domain.contact.share.ContactType;
import com.example.javacase.domain.enterprise.entity.Enterprise;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static org.springframework.util.Assert.notNull;

@Entity
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @NotNull
    private ContactType type;

    @Column
    @NotBlank
    private String firstname;

    @Column
    @NotBlank
    private String lastname;

    @Column
    @NotBlank
    private String address;

    @Column
    @Size(min = 12, max = 12, message = "la tva en belgique est sur 12 caractères")
    private String vat;

    protected Contact() {
        // Used by hibernate
    }

    public Contact(ContactType type, String firstname, String lastname, String address, String vat) {
        notNull(type, "type cannot be null");
        notNull(firstname, "firstname cannot be null");
        notNull(lastname, "lastname cannot be null");
        notNull(address, "address cannot be null");
        this.type = type;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
        this.vat = vat;
    }

    public Long getId() {
        return id;
    }

    public ContactType getType() {
        return type;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getAddress() {
        return address;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public void setType(ContactType type) {
        this.type = type;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("type", type)
                .append("firstname", firstname)
                .append("lastname", lastname)
                .append("address", address)
                .append("vat", vat)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Contact)) return false;

        Contact contact = (Contact) o;

        return new EqualsBuilder()
                .append(id, contact.id)
                .append(type, contact.type)
                .append(firstname, contact.firstname)
                .append(lastname, contact.lastname)
                .append(address, contact.address)
                .append(vat, contact.vat)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(type)
                .append(firstname)
                .append(lastname)
                .append(address)
                .append(vat)
                .toHashCode();
    }
}
