package com.example.javacase.domain.contact.share;

public enum ContactType {

    EMPLOYED,
    FREELANCE

}
