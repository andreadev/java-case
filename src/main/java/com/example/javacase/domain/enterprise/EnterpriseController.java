package com.example.javacase.domain.enterprise;

import com.example.javacase.domain.contact.entity.Contact;
import com.example.javacase.domain.contact.repository.ContactRepository;
import com.example.javacase.domain.enterprise.converter.EnterpriseConverter;
import com.example.javacase.domain.enterprise.dto.EnterpriseFormDto;
import com.example.javacase.domain.enterprise.entity.Enterprise;
import com.example.javacase.domain.enterprise.repository.EnterpriseRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.net.URI;

/**
 * The web service that takes care of enterprise.
 *
 * @author andrea.benetti
 * @since 1.0
 */
@RestController
@RequestMapping(value = "/enterprise/", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin
public class EnterpriseController {

    private final EnterpriseRepository enterpriseRepository;
    private final EnterpriseConverter enterpriseConverter;
    private final ContactRepository contactRepository;

    public EnterpriseController(EnterpriseRepository enterpriseRepository, EnterpriseConverter enterpriseConverter, ContactRepository contactRepository) {
        this.enterpriseRepository = enterpriseRepository;
        this.enterpriseConverter = enterpriseConverter;
        this.contactRepository = contactRepository;
    }


    /**
     * This entry point is used to create an enterprise.
     *
     * @param enterpriseFormDto the enterprise form
     * @return the response with the created enterprise
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Enterprise> createEnterprise(@Valid @RequestBody EnterpriseFormDto enterpriseFormDto) {

        Enterprise enterprise = enterpriseConverter.convert(enterpriseFormDto);

        Enterprise enterpriseCreated = enterpriseRepository.save(enterprise);

        return ResponseEntity.created(URI.create("/enterprise/" + enterpriseCreated.getId()))
                .body(enterpriseCreated);
    }

    /**
     * This entry point is used to update an enterprise.
     *
     * @param enterpriseId the id of enterprise to edit
     * @param enterpriseFormDto the enterprise form
     * @return the response with the updated enterprise
     */
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Enterprise> updateEnterprise(
            @PathVariable(value = "id") Long enterpriseId,
            @Valid @RequestBody EnterpriseFormDto enterpriseFormDto) {

        Enterprise enterpriseFound = enterpriseRepository.findById(enterpriseId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Enterprise not found for this id : " + enterpriseId));

        Enterprise newValue = enterpriseConverter.convert(enterpriseFormDto);
        enterpriseFound.setAddress(newValue.getAddress());
        enterpriseFound.setVat(newValue.getVat());
        Enterprise enterpriseUpdated = enterpriseRepository.save(enterpriseFound);

        return ResponseEntity.ok(enterpriseUpdated);
    }
    /**
     * This entry point is used to update an enterprise.
     *
     * @param enterpriseId the id of enterprise
     * @param contactId the id of the contact
     * @return the response with the updated enterprise
     */
    @PatchMapping("/{enterpriseId}/{contactId}")
    public ResponseEntity<Enterprise> addContactToEnterprise(
            @PathVariable(value = "enterpriseId") Long enterpriseId,
            @PathVariable(value = "contactId") Long contactId) {

        Enterprise enterpriseFound = enterpriseRepository.findById(enterpriseId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Enterprise not found for this id : " + enterpriseId));

        Contact contactFound = contactRepository.findById(contactId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Contact not found for this id : " + contactId));

        if (!enterpriseFound.getContacts().contains(contactFound)) {
            enterpriseFound.addContact(contactFound);
            enterpriseRepository.save(enterpriseFound);
        }

        return ResponseEntity.ok(enterpriseFound);
    }
}
