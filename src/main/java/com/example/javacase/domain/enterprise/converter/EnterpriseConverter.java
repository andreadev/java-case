package com.example.javacase.domain.enterprise.converter;

import com.example.javacase.domain.enterprise.dto.EnterpriseFormDto;
import com.example.javacase.domain.enterprise.entity.Enterprise;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class EnterpriseConverter implements Converter<EnterpriseFormDto, Enterprise> {

    @Override
    public Enterprise convert(EnterpriseFormDto enterpriseFormDto) {
        return new Enterprise(enterpriseFormDto.getVat(), enterpriseFormDto.getAddress());
    }

}
