package com.example.javacase.domain.enterprise.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class EnterpriseFormDto implements Serializable {

    @NotNull
    @Size(min = 12, max = 12, message = "la tva en belgique est sur 12 caractères")
    private final String vat;
    @NotBlank
    private final String address;

    public EnterpriseFormDto(String vat, String address) {
        this.vat = vat;
        this.address = address;
    }

    public String getVat() {
        return vat;
    }

    public String getAddress() {
        return address;
    }
}
