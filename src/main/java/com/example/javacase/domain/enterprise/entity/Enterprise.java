package com.example.javacase.domain.enterprise.entity;

import com.example.javacase.domain.contact.entity.Contact;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.springframework.util.Assert.notNull;

@Entity
public class Enterprise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany
    private final List<Contact> contacts = new ArrayList<>();

    @Column
    @NotNull
    @Size(min = 12, max = 12, message = "la tva en belgique est sur 12 caractères")
    private String vat;

    @Column
    @NotBlank
    private String address;

    protected Enterprise() {
        // Used by hibernate
    }

    public Enterprise(String vat, String address) {
        notNull(vat, "vat cannot be null");
        notNull(address, "address cannot be null");
        this.vat = vat;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Contact> getContacts() {
        return Collections.unmodifiableList(contacts);
    }

    public void addContact(Contact contact) {
        notNull(contact, "Contact cannot be null");
        this.contacts.add(contact);
    }

    public void removeContact(Contact contact) {
        notNull(contact, "Contact cannot be null");
        this.contacts.removeAll(Collections.singletonList(contact));
    }


    public String getVat() {
        return vat;
    }

    public void setVat(String tva) {
        this.vat = tva;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("contacts", contacts)
                .append("vat", vat)
                .append("address", address)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Enterprise)) return false;

        Enterprise that = (Enterprise) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(contacts, that.contacts)
                .append(vat, that.vat)
                .append(address, that.address)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(contacts)
                .append(vat)
                .append(address)
                .toHashCode();
    }
}
