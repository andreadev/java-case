package com.example.javacase.domain.enterprise.repository;

import com.example.javacase.domain.contact.entity.Contact;
import com.example.javacase.domain.enterprise.entity.Enterprise;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EnterpriseRepository extends JpaRepository<Enterprise, Long> {

    List<Enterprise> findAllByContactsContains(Contact contact);
}
