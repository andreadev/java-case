create table if not exists enterprise (
    id bigserial primary key,
    vat text not null,
    address text not null
);
create table if not exists contact (
    id bigserial primary key,
    type text not null,
    firstname text not null,
    lastname text not null,
    address text not null,
    vat text
);
create table if not exists enterprise_contacts (
    enterprise_id bigint references enterprise(id),
    contacts_id bigint references contact(id)
);