package com.example.javacase;

import com.example.javacase.domain.contact.dto.ContactFormDto;
import com.example.javacase.domain.contact.entity.Contact;
import com.example.javacase.domain.contact.share.ContactType;
import com.example.javacase.domain.enterprise.dto.EnterpriseFormDto;
import com.example.javacase.domain.enterprise.entity.Enterprise;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "/scripts/clean-all.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "/scripts/clean-all.sql")
class JavaCaseApplicationTests {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @BeforeEach
    public void setup() {
        testRestTemplate.getRestTemplate().setRequestFactory(new HttpComponentsClientHttpRequestFactory());
    }

    @Test
    void test_createEnterprise() {

        EnterpriseFormDto enterpriseFormDto = new EnterpriseFormDto("0123.456.789", "enterprise street");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<EnterpriseFormDto> requestEntity = new HttpEntity<>(enterpriseFormDto, headers);

        ResponseEntity<Enterprise> responseEntity = testRestTemplate.postForEntity("/enterprise/", requestEntity, Enterprise.class);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertNotNull(responseEntity.getBody().getId());
        assertEquals("0123.456.789", responseEntity.getBody().getVat());
        assertEquals("enterprise street", responseEntity.getBody().getAddress());
        assertEquals(0, responseEntity.getBody().getContacts().size());
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {
            "/scripts/clean-all.sql",
            "/scripts/insert-enterprise.sql"
    })
    void test_updateEnterprise() {

        EnterpriseFormDto enterpriseFormDto = new EnterpriseFormDto("0123.456.789", "enterprise street");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<EnterpriseFormDto> requestEntity = new HttpEntity<>(enterpriseFormDto, headers);

        ResponseEntity<Enterprise> responseEntity = testRestTemplate.exchange("/enterprise/1", HttpMethod.PUT, requestEntity, Enterprise.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertNotNull(responseEntity.getBody().getId());
        assertEquals("0123.456.789", responseEntity.getBody().getVat());
        assertEquals("enterprise street", responseEntity.getBody().getAddress());
        assertEquals(0, responseEntity.getBody().getContacts().size());

    }



    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {
            "/scripts/clean-all.sql",
            "/scripts/insert-enterprise.sql",
            "/scripts/insert-contact.sql"
    })
    void test_addContactToEnterprise() {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<EnterpriseFormDto> requestEntity = new HttpEntity<>(headers);

        ResponseEntity<Enterprise> responseEntity = testRestTemplate.exchange("/enterprise/1/1", HttpMethod.PATCH, requestEntity, Enterprise.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertNotNull(responseEntity.getBody().getId());
        assertEquals("0123.456.888", responseEntity.getBody().getVat());
        assertEquals("enterprise street sql", responseEntity.getBody().getAddress());
        assertEquals(1, responseEntity.getBody().getContacts().size());
        assertEquals(1, responseEntity.getBody().getContacts().get(0).getId());
        assertEquals("Tom", responseEntity.getBody().getContacts().get(0).getFirstname());
        assertEquals("Morello", responseEntity.getBody().getContacts().get(0).getLastname());
        assertEquals(ContactType.EMPLOYED, responseEntity.getBody().getContacts().get(0).getType());
        assertEquals("contact street", responseEntity.getBody().getContacts().get(0).getAddress());
        assertNull(responseEntity.getBody().getContacts().get(0).getVat());

    }

    @Test
    void test_createContact() {

        ContactFormDto contactFormDto = new ContactFormDto(ContactType.EMPLOYED, "Andrea", "Benetti", "Rue de Belgique", null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<ContactFormDto> requestEntity = new HttpEntity<>(contactFormDto, headers);

        ResponseEntity<Contact> responseEntity = testRestTemplate.postForEntity("/contact/", requestEntity, Contact.class);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertNotNull(responseEntity.getBody().getId());
        assertEquals(1, responseEntity.getBody().getId());
        assertEquals(ContactType.EMPLOYED, responseEntity.getBody().getType());
        assertEquals("Andrea", responseEntity.getBody().getFirstname());
        assertEquals("Benetti", responseEntity.getBody().getLastname());
        assertEquals("Rue de Belgique", responseEntity.getBody().getAddress());
        assertNull(responseEntity.getBody().getVat());

    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {
            "/scripts/clean-all.sql",
            "/scripts/insert-contact.sql"
    })
    void test_updateContact() {

        ContactFormDto contactFormDto = new ContactFormDto(ContactType.EMPLOYED, "Andrea", "Benetti", "Rue de Belgique", null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<ContactFormDto> requestEntity = new HttpEntity<>(contactFormDto, headers);

        ResponseEntity<Contact> responseEntity = testRestTemplate.exchange("/contact/1", HttpMethod.PUT, requestEntity, Contact.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertNotNull(responseEntity.getBody().getId());
        assertEquals(1, responseEntity.getBody().getId());
        assertEquals(ContactType.EMPLOYED, responseEntity.getBody().getType());
        assertEquals("Andrea", responseEntity.getBody().getFirstname());
        assertEquals("Benetti", responseEntity.getBody().getLastname());
        assertEquals("Rue de Belgique", responseEntity.getBody().getAddress());
        assertNull(responseEntity.getBody().getVat());

    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {
            "/scripts/clean-all.sql",
            "/scripts/insert-contact.sql"
    })
    void test_deleteContact() {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<ContactFormDto> requestEntity = new HttpEntity<>(headers);

        ResponseEntity<Contact> responseEntity = testRestTemplate.exchange("/contact/1", HttpMethod.DELETE, requestEntity, Contact.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertNotNull(responseEntity.getBody().getId());
        assertEquals(1, responseEntity.getBody().getId());
        assertEquals(ContactType.EMPLOYED, responseEntity.getBody().getType());
        assertEquals("Tom", responseEntity.getBody().getFirstname());
        assertEquals("Morello", responseEntity.getBody().getLastname());
        assertEquals("contact street", responseEntity.getBody().getAddress());
        assertNull(responseEntity.getBody().getVat());

    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {
            "/scripts/clean-all.sql",
            "/scripts/insert-enterprise.sql",
            "/scripts/insert-contact.sql",
            "/scripts/link-contact-to-enterprise.sql"
    })
    void test_deleteContactLinkedToEnterprise() {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<ContactFormDto> requestEntity = new HttpEntity<>(headers);

        ResponseEntity<Contact> responseEntity = testRestTemplate.exchange("/contact/1", HttpMethod.DELETE, requestEntity, Contact.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertNotNull(responseEntity.getBody().getId());
        assertEquals(1, responseEntity.getBody().getId());
        assertEquals(ContactType.EMPLOYED, responseEntity.getBody().getType());
        assertEquals("Tom", responseEntity.getBody().getFirstname());
        assertEquals("Morello", responseEntity.getBody().getLastname());
        assertEquals("contact street", responseEntity.getBody().getAddress());
        assertNull(responseEntity.getBody().getVat());

    }

}
